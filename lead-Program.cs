//Kenneth Long
//10.27.15
//Extract/build full paths from dir /s > dir.txt, no support for | yet
//requires:
//source dir lisitng filename (dir.txt)
//pattern (.py)
//lead dir.txt .py -n
//-p output parent directories to stdout
//-n show count

using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO;


namespace extract_and_build_paths
{
    class Program
    {

        class web_variable
        {
            
        }

        class web_variable_path
        {
            String web_path;
            List<String> variables;
        }

        static List<String> m_paths = new List<String>();


                
        static void get_files(String d, String pattern, bool show_numbers, bool show_parent_dir)
        {
                //over the 'string' that is the entire text of the file
                int start = 0, end = 0;
                while (end < d.Length)
                {
                    //from the current 'start' of the string
                    //look for the wwwroot directory as the first delim
                    //id_directory_name

                    //generic dos/windows dir listing (win7)
                    start = d.IndexOf("Directory of ", start);
                    if ( start < 0 )
                        break;
                    
                    //keep drive letter
                    //when starting with \\id directory name
                    //start -= 2;

                    start += 13;

                    //look for the eol as the term delim
                    end = d.IndexOf("\n", start);

                    //this is the dir prefix
                    String prefix = d.Substring(start, end - start-1)+'\\';

                    //output dir for trace verification
                    if(show_parent_dir)
                        Console.Out.Write(prefix +  "\n");

                    //get index of new line
                    start = d.IndexOf("\n", end);

                    start = end+2;

                    //for all the files in the directory
                    do
                    {
                        //get the last space after the size string
                        start = d.LastIndexOf(" ", start+42, 42)+1;

                        //get the newline index
                        end = d.IndexOf("\n", start);

                        //get the file name
                        String file = d.Substring(start, end - start-1);
                        

                        start = end;

                        if (file.Contains(pattern))
                        {
                            //store the path string
                            m_paths.Add(prefix + file);
                            //until the end of the listing where the count
                            //of files occurs
                            Console.Out.Write((show_numbers ? m_paths.Count + ": " : "") + m_paths[m_paths.Count - 1] + "\n");
                        }
                    } while (d[start+1] != ' ');
                }

        }

        static void Main(string[] args)
        {
            String filename = "";
            String pattern = "*";
            bool show_numbers = false;
            bool show_parent_dir_path = false;
            if (args.Length < 2)
            {
                Console.Out.Write("\nsource filename required");
                Console.Out.Write("\npattern required");
                
                return;
            }
            else
            {
                filename = args[0];
                pattern = args[1];
                if (pattern.CompareTo("*")==0)
                    pattern = "";

                //just a poorman argparse/getopt...
                if (args.Length > 2)
                {
                    foreach (var arg in args)
                    {
                        show_numbers = show_numbers | (arg.CompareTo("-n") == 0);
                        show_parent_dir_path = show_parent_dir_path | (arg.CompareTo("-p") == 0);
                    }
                }

                //Console.Out.Write("Pattern:" + pattern + "\n");

                StreamReader sr = new StreamReader(filename);
                String d = sr.ReadToEnd();

                //Console.Out.Write(d);

                get_files(d, pattern, show_numbers, show_parent_dir_path);

                //get_variables();

            }
        }
    }
}
